# 內部預約系統(2020/9 ~ 2021/1) #

![登入畫面](https://cdn.discordapp.com/attachments/292622123543953408/811536442920665118/b789c3f83b260588.png)

### 開發環境 ###

1. Pug
2. Sass
3. Vue
4. Vuetify
5. Axios

### 簡介 ###

* 內部預約系統採用簡約的設計，利用月曆的方式顯示預約事件，不管是預約會議室還是公務車，使用者都能迅速查看事件，不到 15 秒即可快速預約事件 !
* 此為管理者帳號介面

### 目錄 ###

1. 訪客預約儀錶版
2. 預約功能(Demo 為公務車預約)
3. 休假預約
4. 人事設定

---------------------------------------------------

### 1.訪客預約儀錶版 ###

![訪客儀錶版](https://cdn.discordapp.com/attachments/292622123543953408/811536440491245588/3c8175c8c02028e2.png)
顯示當日的訪客預約，可快速查看或更新訪客狀態

### 2.預約功能 ###

![公務車預約 詳細事件](https://cdn.discordapp.com/attachments/292622123543953408/812223427741745192/FireShot_Capture_949_-_-_10.11.101.99.png)
Demo 為公務車預約，利用顏色區分事件期限，點擊事件後可查看詳細事件

![公務車預約 more](https://cdn.discordapp.com/attachments/292622123543953408/812172135295352912/FireShot_Capture_952_-_-_10.11.101.99.png)
當天如果太多預約，點擊 more 可顯示當日事件

### 3.休假預約 ###

![今日總覽](https://cdn.discordapp.com/attachments/292622123543953408/811536434426937434/747105a16f85864c.png)
顯示當日的休假，可快速查看假單

![請假列表](https://cdn.discordapp.com/attachments/292622123543953408/811536427727978506/408c52b2009a1d94.png)
管理者可查看所有成員；主管可查看所屬部門成員；一般使用者只能看自己

![休假](https://cdn.discordapp.com/attachments/292622123543953408/811536445617078332/bcba6c408e76ba53.png)
選擇日期後會篩選出可選擇的代理人，並計算出請假時數與請假規則

![休假](https://cdn.discordapp.com/attachments/292622123543953408/812207988336230420/1611743518723.gif)
Demo 實際操作，選擇日期後會篩選出可選擇的代理人，並計算出請假時數與請假規則

![簽核進度](https://cdn.discordapp.com/attachments/292622123543953408/811536438167470080/61322d9789c01d29.png)
點擊請假進度可查看請假流程

### 4.人事設定 ###

![人事設定](https://cdn.discordapp.com/attachments/292622123543953408/812214285794803732/223b7100ca8a968a.png)
左邊用 treeview 的方式顯示部門->員工，右邊為員工資料顯示